/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package colecciones;

/**
 *
 * @author estudiante
 */
public class ListaCD<T> {

    private NodoD<T> cabeza;
    private int tamanio;

    public ListaCD() {

        this.cabeza = new NodoD();
        this.cabeza.setSig(cabeza);
        this.cabeza.setAnt(cabeza);
        //sobra:
        this.cabeza.setInfo(null);

    }

    public void insertarInicio(T info) {
        NodoD<T> nuevo = new NodoD<T>(info, this.cabeza.getSig(), this.cabeza);
        //redireccionar
        this.cabeza.getSig().setAnt(nuevo);
        this.cabeza.setSig(nuevo);

        this.tamanio++;
    }

    //O(1)
    public void insertarFinal(T info) {
        NodoD<T> nuevo = new NodoD<T>(info, this.cabeza, this.cabeza.getAnt());
        this.cabeza.setAnt(nuevo);
        nuevo.getAnt().setSig(nuevo);
        this.tamanio++;
    }

    public void eliminar(int i) throws Exception {
        if (i >= this.tamanio || this.esVacio() || i < 0) {
            throw new Exception("No se puede eliminar");
        } else {
            NodoD<T> nuevo = this.cabeza.getSig();
            for (int j = 0; j <= i; j++) {
                if (i == j) {
                    nuevo.getAnt().setSig(nuevo.getSig());
                    nuevo.getSig().setAnt(nuevo.getAnt());
                    tamanio--;
                } else {
                    nuevo = nuevo.getSig();
                }
            }
        }
    }

    public boolean contiene_Elemen_Repeti(ListaCD<T> l2) throws Exception {
        if (this.esVacio() || l2.esVacio() || this.tamanio != l2.tamanio) {
            throw new Exception("La lista esta vacia");
        }

        NodoD<T> n2 = l2.cabeza.getSig();
        for (NodoD<T> n1 = this.cabeza.getSig(); n1 != this.cabeza; n1 = n1.getSig(), n2 = n2.getSig()) {
            if (n1.getInfo() != n2.getInfo()) {
                return false;
            }
        }
        return true;
    }

    public String toString() {
        String msg = "";
        for (NodoD<T> x = this.cabeza.getSig(); x != this.cabeza; x = x.getSig()) {
            msg += x.getInfo() + "<->";
        }
        return msg;
    }

    public boolean esVacio() {

        return (this.cabeza == this.cabeza.getSig());
        //return (this.cabeza==this.cabeza.getAnt());
        //return this.tamanio==0;
    }

    public int getTamanio() {
        return tamanio;
    }

}
